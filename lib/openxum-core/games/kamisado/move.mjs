"use strict";

import OpenXum from '../../openxum/index.mjs';
import MoveType from './move_type.mjs';

class Move extends OpenXum.Move {

  constructor(ty, f, t) {
    super();
    this._type = ty;
    this._from = f;
    this._to = t;
  }

  // public methods
  decode(str) {
    if (str[0] === 'M') {
      this._type = MoveType.MOVE;
    } else {
      this._type = MoveType.PASS;
    }
    this._from = {
      x: str.charCodeAt(1) - 'a'.charCodeAt(0),
      y: str.charCodeAt(2) - '1'.charCodeAt(0)
    };
    this._to = {
      x: str.charCodeAt(3) - 'a'.charCodeAt(0),
      y: str.charCodeAt(4) - '1'.charCodeAt(0)
    };
  }

  encode() {
    return (this._type === MoveType.MOVE ? 'M' : 'P') + String.fromCharCode('a'.charCodeAt(0) + this._from.x) + (this._from.y + 1) +
      String.fromCharCode('a'.charCodeAt(0) + this._to.x) + (this._to.y + 1);
  }

  from() {
    return this._from;
  }

  from_object(data) {
    this._type = data.type;
    this._from = {x: data.from.x, y: data.from.y};
    this._to = {x: data.to.x, y: data.to.y};
  }

  to() {
    return this._to;
  }

  to_object() {
    return {type: this._type, from: this._from, to: this._to};
  }

  to_string() {
    if (this._type === MoveType.MOVE) {
      return 'move tower from ' + String.fromCharCode('a'.charCodeAt(0) + this._from.x) + (this._from.y + 1) +
        ' to ' + String.fromCharCode('a'.charCodeAt(0) + this._to.x) + (this._to.y + 1);
    } else {
      return "pass";
    }
  }

  type() {
    return this._type;
  }
}

export default Move;